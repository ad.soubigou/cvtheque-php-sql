<?php
// Calcul de l'âge en fonction de la date de naissance
function calculAge($datenaiss)
{
    $date = date_create($datenaiss);
    $date->format('d-m-y');
    $dateJour = date_create("now");
    $dateJour->format('d-m-y');
    $calcul = date_diff($date, $dateJour);
    return $calcul->format('%y ans');
}

// Fonction de connexion à la base de données
function connectDB()
{
    try {
        $dbh = new PDO('mysql:host=localhost;dbname=cvtheque', "root", "");
        return $dbh;
    } catch (PDOException $e) {
        // tenter de réessayer la connexion après un certain délai, par exemple
        print "Impossible de se connecter";
    }
}

// Récupération de tous les candidats présents dans la base de données
function selectCandidat()
{
    $dbh = connectDB();
    try {
        $stmt = $dbh->prepare("SELECT * FROM `candidats`");
        $result = $stmt->execute();
        $tabCards = $stmt->fetchAll();
    } catch (\PDOException $e) {
        echo $e->getMessage() . '<BR>';
    }
    return $tabCards;
}

// Récupération des compétences du candidat dans la base de données
function selectSkills($data)
{
    $dbh = connectDB();
    try {
        $stmt = $dbh->prepare("SELECT `nomCompetences` FROM `competences` JOIN `candidatscompetences` ON `competences`.`idCompetences` = `candidatscompetences`.`idCompetences` JOIN `candidats` ON `candidatscompetences`.`idCandidat` = `candidats`.`idCandidat` WHERE `candidatscompetences`.`idCandidat` = $data;");
        $result = $stmt->execute();
        $tabComp = $stmt->fetchAll();
    } catch (\PDOException $e) {
        echo $e->getMessage() . '<BR>';
    }
    $dbh = null;
    return $tabComp;
}


// Fonction de tri
function sortCards($select)
{
    $dbh = connectDB();
    switch ($select) {
        case '0':
            selectCandidat();
            break;
        case '1':
            try {
                $stmt = $dbh->prepare("SELECT * FROM `candidats` ORDER BY `nomCandidat` ASC");
                $result = $stmt->execute();
                $tabCards = $stmt->fetchAll();
            } catch (\PDOException $e) {
                echo $e->getMessage() . '<BR>';
            }
            break;
        case '2':
            try {
                $stmt = $dbh->prepare("SELECT * FROM `candidats` ORDER BY `nomCandidat` DESC");
                $result = $stmt->execute();
                $tabCards = $stmt->fetchAll();
            } catch (\PDOException $e) {
                echo $e->getMessage() . '<BR>';
            }
            break;
        case '3':
            try {
                $stmt = $dbh->prepare("SELECT * FROM `candidats` ORDER BY `villeCandidat` ASC");
                $result = $stmt->execute();
                $tabCards = $stmt->fetchAll();
            } catch (\PDOException $e) {
                echo $e->getMessage() . '<BR>';
            }
            break;
        case '4':
            try {
                $stmt = $dbh->prepare("SELECT * FROM `candidats` ORDER BY `villeCandidat` DESC");
                $result = $stmt->execute();
                $tabCards = $stmt->fetchAll();
            } catch (\PDOException $e) {
                echo $e->getMessage() . '<BR>';
            }
            break;
        case '5':
            try {
                $stmt = $dbh->prepare("SELECT * FROM `candidats` ORDER BY `profilCandidat` ASC");
                $result = $stmt->execute();
                $tabCards = $stmt->fetchAll();
            } catch (\PDOException $e) {
                echo $e->getMessage() . '<BR>';
            }
            break;
        case '6':
            try {
                $stmt = $dbh->prepare("SELECT * FROM `candidats` ORDER BY `profilCandidat` DESC");
                $result = $stmt->execute();
                $tabCards = $stmt->fetchAll();
            } catch (\PDOException $e) {
                echo $e->getMessage() . '<BR>';
            }
            break;
        case '7':
            try {
                $stmt = $dbh->prepare("SELECT * FROM `candidats` ORDER BY `dobCandidat` DESC");
                $result = $stmt->execute();
                $tabCards = $stmt->fetchAll();
            } catch (\PDOException $e) {
                echo $e->getMessage() . '<BR>';
            }
            break;
        case '8':
            try {
                $stmt = $dbh->prepare("SELECT * FROM `candidats` ORDER BY `dobCandidat` ASC");
                $result = $stmt->execute();
                $tabCards = $stmt->fetchAll();
            } catch (\PDOException $e) {
                echo $e->getMessage() . '<BR>';
            }
            break;
    }
    $dbh = null;
    return $tabCards;
}
