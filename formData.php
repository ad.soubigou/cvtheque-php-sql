<?php
require_once('./functions.php');
// Cas de la modification d'une card
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_GET['id'])) {
    $id = $_GET["id"];
    // Récupération et filtrage des données du formulaire
    $name = htmlspecialchars($_POST["name"], ENT_SUBSTITUTE | ENT_HTML5);
    $firstname = htmlspecialchars($_POST["firstname"], ENT_SUBSTITUTE | ENT_HTML5);
    $birthdate = $_POST["birthdate"];
    $adress = htmlspecialchars($_POST["adress"], ENT_SUBSTITUTE | ENT_HTML5);
    $adress_add = htmlspecialchars($_POST["adress-add"], ENT_SUBSTITUTE | ENT_HTML5);
    $code = htmlspecialchars($_POST["code"]);
    $city = htmlspecialchars($_POST["city"], ENT_SUBSTITUTE | ENT_HTML5);
    $email = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
    $phone = filter_var($_POST["phone"], FILTER_SANITIZE_NUMBER_INT);
    $cellphone = filter_var($_POST["cellphone"], FILTER_SANITIZE_NUMBER_INT);
    $profile = htmlspecialchars($_POST["profile"], ENT_SUBSTITUTE | ENT_HTML5);
    $skills = array();
    for ($i = 1; $i <= 10; $i++) {
        $skills[$i] = htmlspecialchars($_POST["skills$i"], ENT_SUBSTITUTE | ENT_HTML5);
    }
    $website = filter_var($_POST["website"], FILTER_SANITIZE_URL);
    $linkedin = filter_var($_POST["linkedin"], FILTER_SANITIZE_URL);
    $viadeo = filter_var($_POST["viadeo"], FILTER_SANITIZE_URL);
    $facebook = filter_var($_POST["facebook"], FILTER_SANITIZE_URL);

    $dbh = connectDB();
    // Modification du candidat
    try {
        $stmt = $dbh->prepare("UPDATE `candidats` SET `nomCandidat`=:nom,`prenomCandidat`=:prenom,`dobCandidat`=:birthdate,`villeCandidat`=:ville,`codeCandidat`=:code,`adresseCandidat`=:adresse,`adresscompCandidat`=:adressecomp,`cellCandidat`=:portable,`phoneCandidat`=:telephone,`emailCandidat`=:email,`profilCandidat`=:profil,`siteCandidat`=:website,`linkedinCandidat`=:linkedin,`viadeoCandidat`=:viadeo,`facebookCandidat`=:facebook WHERE `idCandidat` = $id");
        $stmt->bindValue(':nom', $name);
        $stmt->bindValue(':prenom', $firstname);
        $stmt->bindValue(':birthdate', $birthdate);
        $stmt->bindValue(':ville', $city);
        $stmt->bindValue(':code', $code);
        $stmt->bindValue(':adresse', $adress);
        $stmt->bindValue(':adressecomp', $adress_add);
        $stmt->bindValue(':portable', $cellphone);
        $stmt->bindValue(':telephone', $phone);
        $stmt->bindValue(':email', $email);
        $stmt->bindValue(':profil', $profile);
        $stmt->bindValue(':website', $website);
        $stmt->bindValue(':linkedin', $linkedin);
        $stmt->bindValue(':viadeo', $viadeo);
        $stmt->bindValue(':facebook', $facebook);
        $result = $stmt->execute();
    } catch (\PDOException $e) {
        echo $e->getMessage() . '<BR>';
    }
    $dbh = null;
    $dbh = connectDB();
    // Suppression du lien entre le candidat et les compétences
    try {
        $stmt = $dbh->prepare("DELETE FROM `candidatscompetences` WHERE `idCandidat` = $id");
        $result = $stmt->execute();
    } catch (\PDOException $e) {
        echo $e->getMessage() . '<BR>';
    }
    $dbh = null;
    $dbh = connectDB();
    // Création d'un nouveau lien entre le candidat et les compétences
    foreach ($skills as $skill) {
        if ($skill !== '') {
            // On vérifie si la compétence existe déjà dans la base de données
            try {
                $stmt = $dbh->prepare("SELECT * FROM `competences` WHERE `nomCompetences` = :nomCompetences");
                $stmt->bindValue(':nomCompetences', $skill);
                $result = $stmt->execute();
                // Si la compétence $skill existe dans la base, on l'insère dans un tableau
                $tabCompetences = $stmt->fetchAll();
            } catch (PDOException $e) {
                echo $e->getMessage() . '<br>';
            }
            // Si le tableau créé est vide, la compétence n'existe pas et on ajoute l'ajoute à la base de données
            if (count($tabCompetences) == 0) {
                try {
                    $stmt = $dbh->prepare("INSERT INTO `competences`(`nomCompetences`) VALUES (:nomCompetences)");
                    $stmt->bindValue(':nomCompetences', $skill);
                    $stmt->execute();
                    $idComp = $dbh->lastInsertId();
                } catch (PDOException $e) {
                    echo $e->getMessage() . '<br>';
                }
            }
            // On récupère l'id de la compétence dans la table compétences
            try {
                $stmt = $dbh->prepare("SELECT `idCompetences` FROM `competences` WHERE `nomCompetences` = :nomCompetences");
                $stmt->bindValue(':nomCompetences', $skill);
                $stmt->execute();
                $idComp = $stmt->fetch();
            } catch (PDOException $e) {
                echo $e->getMessage() . '<br>';
            }
            // On insère l'id candidat et les id compétences dans la table candidatcompetence
            try {
                $stmt = $dbh->prepare("INSERT INTO `candidatscompetences`(`idCandidat`, `idCompetences`) VALUES (:idcandidat,:idcompetence)");
                $stmt->bindValue(':idcandidat', $id);
                $stmt->bindValue(':idcompetence', $idComp[0]);
                $stmt->execute();
            } catch (PDOException $e) {
                echo $e->getMessage() . '<br>';
            }
        }
    }
    $dbh = null;

    // Récupération du cv
    if (isset($_FILES) && !empty($_FILES["cv"]["name"])) {

        // Choix du dossier où sera enregistré le fichier
        $target_dir = "./cvs/";
        // Obtention du nom du fichier uploadé par l'utilisateur
        $target_file = $target_dir . basename($_FILES["cv"]["name"]);
        // Récupération de l'extension du fichier
        $doctype = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        // Renommage du fichier avec le nouvel ID créé
        $fileName = $target_dir . $id . "." . $doctype;
        $filepdf = $target_dir . $id . ".pdf";
        $filedocx = $target_dir . $id . ".docx";
        $uploadOk = 1;
        // On vérifie si le fichier existe, si oui, on le supprime
        if (file_exists($filepdf)) {
            unlink($filepdf);
        } else if (file_exists($filedocx)) {
            unlink($filedocx);
        }
        // On limite la taille des fichiers que l'on peut uploader
        if ($_FILES["cv"]["size"] > 500000) {
            echo "Veuillez sélectionner un fichier de 500ko ou moins.";
            $uploadOk = 0;
        }
        // On limite le type de fichier
        if ($doctype != "pdf" && $doctype != "docx") {
            echo "Veuillez sélectionner un fichier pdf ou docx.";
            $uploadOk = 0;
        }
        // On vérifie qu'il n'y a pas d'erreur
        if ($uploadOk == 0) {
            echo "Erreur, le fichier n'a pas été uploadé.";
            // Si pas d'erreur, on uploade le fichier
        } else {
            if (move_uploaded_file($_FILES["cv"]["tmp_name"], $fileName)) {
                echo "Le fichier " . htmlspecialchars(basename($_FILES["cv"]["name"])) . " a bien été uploadé.";
            } else {
                echo "Erreur lors de l'upload";
            }
        }
    }

    header("Location: cvtheque.php?updated=1");
}
// Cas de la création d'une card
else if ($_POST) { {
        $name = htmlspecialchars($_POST["name"], ENT_SUBSTITUTE | ENT_HTML5);
        $firstname = htmlspecialchars($_POST["firstname"], ENT_SUBSTITUTE | ENT_HTML5);
        $birthdate = $_POST["birthdate"];
        $adress = htmlspecialchars($_POST["adress"], ENT_SUBSTITUTE | ENT_HTML5);
        $adress_add = htmlspecialchars($_POST["adress-add"], ENT_SUBSTITUTE | ENT_HTML5);
        $code = htmlspecialchars($_POST["code"]);
        $city = htmlspecialchars($_POST["city"], ENT_SUBSTITUTE | ENT_HTML5);
        $email = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
        $phone = filter_var($_POST["phone"], FILTER_SANITIZE_NUMBER_INT);
        $cellphone = filter_var($_POST["cellphone"], FILTER_SANITIZE_NUMBER_INT);
        $profile = htmlspecialchars($_POST["profile"], ENT_SUBSTITUTE | ENT_HTML5);
        $skills = array();
        for ($i = 1; $i <= 10; $i++) {
            $skills[$i] = htmlspecialchars(strtolower($_POST["skills$i"]), ENT_SUBSTITUTE | ENT_HTML5);
        }
        $website = filter_var($_POST["website"], FILTER_SANITIZE_URL);
        $linkedin = filter_var($_POST["linkedin"], FILTER_SANITIZE_URL);
        $viadeo = filter_var($_POST["viadeo"], FILTER_SANITIZE_URL);
        $facebook = filter_var($_POST["facebook"], FILTER_SANITIZE_URL);
    };

    // Poster un nouveau candidat dans la base de données
    $dbh = connectDB();
    try {
        $stmt = $dbh->prepare("INSERT INTO `candidats`(`nomCandidat`, `prenomCandidat`, `dobCandidat`, `villeCandidat`, `codeCandidat`, `adresseCandidat`, `adresscompCandidat`, `cellCandidat`, `phoneCandidat`, `emailCandidat`, `profilCandidat`, `siteCandidat`, `linkedinCandidat`, `viadeoCandidat`, `facebookCandidat`) VALUES (:nom,:prenom,:birthdate,:ville,:code,:adresse,:adressecomp,:portable,:telephone,:email,:profil,:website,:linkedin,:viadeo,:facebook)");
        $stmt->bindValue(':nom', $name);
        $stmt->bindValue(':prenom', $firstname);
        $stmt->bindValue(':birthdate', $birthdate);
        $stmt->bindValue(':ville', $city);
        $stmt->bindValue(':code', $code);
        $stmt->bindValue(':adresse', $adress);
        $stmt->bindValue(':adressecomp', $adress_add);
        $stmt->bindValue(':portable', $cellphone);
        $stmt->bindValue(':telephone', $phone);
        $stmt->bindValue(':email', $email);
        $stmt->bindValue(':profil', $profile);
        $stmt->bindValue(':website', $website);
        $stmt->bindValue(':linkedin', $linkedin);
        $stmt->bindValue(':viadeo', $viadeo);
        $stmt->bindValue(':facebook', $facebook);

        $result = $stmt->execute();
        $idCandidat = $dbh->lastInsertId();
    } catch (\PDOException $e) {
        echo $e->getMessage() . '<BR>';
    }
    $dbh = null;

    // Poster les nouvelles compétences dans la base de données
    $dbh = connectDB();
    foreach ($skills as $skill) {
        if ($skill !== '') {
            // On vérifie si la compétence existe déjà dans la base de données
            try {
                $stmt = $dbh->prepare("SELECT * FROM `competences` WHERE `nomCompetences` = :nomCompetences");
                $stmt->bindValue(':nomCompetences', $skill);
                $result = $stmt->execute();
                // Si la compétence $skill existe dans la base, on l'insère dans un tableau
                $tabCompetences = $stmt->fetchAll();
            } catch (PDOException $e) {
                echo $e->getMessage() . '<br>';
            }
            // Si le tableau créé est vide, la compétence n'existe pas et on ajoute l'ajoute à la base de données
            if (count($tabCompetences) == 0) {
                try {
                    $stmt = $dbh->prepare("INSERT INTO `competences`(`nomCompetences`) VALUES (:nomCompetences)");
                    $stmt->bindValue(':nomCompetences', $skill);
                    $stmt->execute();
                    $idComp = $dbh->lastInsertId();
                } catch (PDOException $e) {
                    echo $e->getMessage() . '<br>';
                }
            }
            try {
                $stmt = $dbh->prepare("SELECT `idCompetences` FROM `competences` WHERE `nomCompetences` = :nomCompetences");
                $stmt->bindValue(':nomCompetences', $skill);
                $stmt->execute();
                $idComp = $stmt->fetch();
            } catch (PDOException $e) {
                echo $e->getMessage() . '<br>';
            }
            try {
                $stmt = $dbh->prepare("INSERT INTO `candidatscompetences`(`idCandidat`, `idCompetences`) VALUES (:idcandidat,:idcompetence)");
                $stmt->bindValue(':idcandidat', $idCandidat);
                $stmt->bindValue(':idcompetence', $idComp[0]);
                $stmt->execute();
            } catch (PDOException $e) {
                echo $e->getMessage() . '<br>';
            }
        }
    }
    $dbh = null;

    // Récupération du cv
    if (isset($_FILES) && !empty($_FILES["cv"]["name"])) {
        // Choix du dossier où sera enregistré le fichier
        $target_dir = "./cvs/";
        // Obtention du nom du fichier uploadé par l'utilisateur
        $target_file = $target_dir . basename($_FILES["cv"]["name"]);
        // Récupération de l'extension du fichier
        $doctype = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        // Renommage du fichier avec le nouvel ID créé
        $fileName = $target_dir . $idCandidat . "." . $doctype;
        $uploadOk = 1;
        // On vérifie si le fichier n'existe pas déjà dans le dossier
        if (file_exists($fileName)) {
            echo "Le fichier existe déjà.";
            $uploadOk = 0;
        }
        // On limite la taille des fichiers que l'on peut uploader
        if ($_FILES["cv"]["size"] > 500000) {
            echo "Veuillez sélectionner un fichier de 500ko ou moins.";
            $uploadOk = 0;
        }
        // On limite le type de fichier
        if ($doctype != "pdf" && $doctype != "docx") { ?>
            <p>Veuillez sélectionner un fichier pdf ou docx.</p>
        <?php
            $uploadOk = 0;
        }

        // On vérifie qu'il n'y a pas d'erreur
        if ($uploadOk == 0) { ?>
            <p>Le fichier n'a pas été uploadé.</p>
            <a href="./update.php">Retour</a>
<?php }
        // Si pas d'erreur, on uploade le fichier
        else {
            if (move_uploaded_file($_FILES["cv"]["tmp_name"], $fileName)) {
                echo "Le fichier " . htmlspecialchars(basename($_FILES["cv"]["name"])) . " a bien été uploadé.";
            } else {
                echo "Erreur lors de l'upload";
            }
        }
    } else {
        $uploadOk = 1;
    }

    header("Location: cvtheque.php?add=1");
} else {
    print "erreur";
}
