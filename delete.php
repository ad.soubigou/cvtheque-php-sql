<?php
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    try {
        $dbh = new PDO('mysql:host=localhost;dbname=cvtheque', "root", "");
    } catch (PDOException $e) {
        // tenter de réessayer la connexion après un certain délai, par exemple
        print "Impossible de se connecter";
    }
    // Suppression du lien entre candidat et ses compétences
    try {
        $stmt = $dbh->prepare("DELETE FROM `candidatscompetences` WHERE `idCandidat` = $id");
        $result = $stmt->execute();
    } catch (\PDOException $e) {
        echo $e->getMessage() . '<BR>';
    }
    $dbh = null;
    try {
        $dbh = new PDO('mysql:host=localhost;dbname=cvtheque', "root", "");
    } catch (PDOException $e) {
        // tenter de réessayer la connexion après un certain délai, par exemple
        print "Impossible de se connecter";
    }
    // Suppression du candidat
    try {
        $stmt = $dbh->prepare("DELETE FROM `candidats` WHERE `idCandidat` = $id");
        $result = $stmt->execute();
    } catch (\PDOException $e) {
        echo $e->getMessage() . '<BR>';
    }
    $dbh = null;

    // Suppression du fichier cv correspondant à la carte
    $pdf = "./cvs/" . $id . ".pdf";
    $docx = "./cvs/" . $id . ".docx";

    if (isset($pdf)) {
        unlink($pdf);
    } else {
        exit;
    }
    if (isset($docx)) {
        unlink($docx);
    } else {
        exit;
    }

    header("Location: cvtheque.php?delete=1");
} else {
    print "Erreur.";
}
