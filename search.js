"use strict";

const searchInput = document.getElementById("searchInput");

function search() {
  // Récupéreration de la recherche entrée par l'utilisateur
  const searchValue = searchInput.value.toLowerCase();
  // Récupération de toutes les cards créées
  const cards = document.querySelectorAll(".flip-card");
  // Parcours des données des cards
  cards.forEach((card) => {
    // Application du filtre de recherche sur les noms et prénoms
    const name = card.querySelector("h5").textContent.toLowerCase();
    // On vérifie si la valeur saisie est contenue dans le champ sélectionné
    if (name.includes(searchValue)) {
      card.style.display = "block";
    } else {
      // On masque les cards qui ne comportent pas la valeur de la saisie
      card.style.display = "none";
    }
  });
}

searchInput.addEventListener("input", search);
