<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,100..700;1,100..700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./style.css" />
    <title>CVthèque - Form</title>
</head>

<body>
    <?php if (isset($_GET['id'])) {
        $id = $_GET["id"];
        try {
            $dbh = new PDO('mysql:host=localhost;dbname=cvtheque', "root", "");
        } catch (PDOException $e) {
            // tenter de réessayer la connexion après un certain délai, par exemple
            print "Impossible de se connecter";
        }
        try {
            $stmt = $dbh->prepare("SELECT * FROM `candidats` WHERE `idCandidat` = $id");
            $result = $stmt->execute();
            $tabCards = $stmt->fetchAll();
        } catch (\PDOException $e) {
            echo $e->getMessage() . '<BR>';
        }
        $dbh = null;
        try {
            $dbh = new PDO('mysql:host=localhost;dbname=cvtheque', "root", "");
        } catch (PDOException $e) {
            // tenter de réessayer la connexion après un certain délai, par exemple
            print "Impossible de se connecter";
        }
        try {
            $stmt = $dbh->prepare("SELECT `nomCompetences` FROM `competences` JOIN `candidatscompetences` ON `competences`.`idCompetences` = `candidatscompetences`.`idCompetences` JOIN `candidats` ON `candidatscompetences`.`idCandidat` = `candidats`.`idCandidat` WHERE `candidatscompetences`.`idCandidat` = $id");
            $result = $stmt->execute();
            $tabComp = $stmt->fetchAll();
        } catch (\PDOException $e) {
            echo $e->getMessage() . '<BR>';
        }
        $dbh = null;
    }

    ?>
    <div class="container p-5 col-xl-6">
        <a href="cvtheque.php">
            <button type="button" class="btn btn-primary mb-3"><i class="fa-solid fa-house"></i></button>
        </a>
        <form action="formData.php<?php isset($id) ? print "?id=" . $id : print "" ?>" method="POST" enctype="multipart/form-data" id="form">
            <div class="mb-3 d-flex justify-content-between">
                <input type="text" name="name" class="form-control" style="width: 48%" placeholder="Nom" value="<?php isset($id) ? print $tabCards[0][1] : print "" ?>" required>
                <input type="text" name="firstname" class="form-control" style="width: 48%" placeholder="Prénom" value="<?php isset($id) ? print $tabCards[0][2] : print "" ?>" required>
            </div>
            <div class="mb-3">
                <label for="birthdate" class="form-label ps-1">Date de naissance :</label>
                <input type="date" name="birthdate" class="form-control" id="birthdate" value="<?php isset($id) ? print $tabCards[0][3] : print "" ?>" required>
            </div>
            <div class="mb-3">
                <textarea name="adress" class="w-100 border rounded p-2" rows="3" placeholder="Adresse"><?php isset($id) ? print $tabCards[0][6] : print "" ?></textarea>
                <textarea name="adress-add" class="w-100 border rounded p-2" rows="3" placeholder="Complément d'adresse"><?php isset($id) ? print $tabCards[0][7] : print "" ?></textarea>
            </div>
            <div class="mb-3 d-flex justify-content-between">
                <input type="text" name="code" class="form-control" style="width: 48%" placeholder="Code postal" value="<?php isset($id) ? print $tabCards[0][5] : print "" ?>">
                <input type="text" name="city" class="form-control" style="width: 48%" placeholder="Ville" value="<?php isset($id) ? print $tabCards[0][4] : print "" ?>">
            </div>
            <div class="mb-3">
                <input type="email" name="email" class="form-control" id="email" placeholder="Courriel" value="<?php isset($id) ? print $tabCards[0][10] : print "" ?>" required>
            </div>
            <div class="mb-3 d-flex justify-content-between">
                <input type="tel" name="phone" class="form-control" style="width: 48%" placeholder="Téléphone fixe" value="<?php isset($id) ? print $tabCards[0][9] : print "" ?>">
                <input type="tel" name="cellphone" class="form-control" style="width: 48%" placeholder="Téléphone portable" value="<?php isset($id) ? print $tabCards[0][8] : print "" ?>" required>
            </div>
            <div class="mb-3">
                <input type="text" name="profile" class="form-control" id="profile" placeholder="Profil" value="<?php isset($id) ? print $tabCards[0][11] : print "" ?>" required>
            </div>
            <div class="mb-3 d-flex flex-wrap w-100">
                <input type="text" id="input-skills" class="form-control" placeholder="Compétences">
                <div id="tags">
                    <!-- Parcours du tableau des skills pour création des tags -->
                    <?php
                    if (isset($tabComp)) {
                        foreach ($tabComp as $key => $comp) {
                    ?>
                            <div class="skill-tag">
                                <?php isset($comp) ? print $comp[0] : "" ?>
                                <button class="delete-btn" name="skills<?php print $key + 1 ?>"><i class="fa-solid fa-xmark"></i></button>
                                <input type="hidden" class="hiddenTag" name="skills<?php print $key + 1 ?>" value="<?php isset($comp) ? print $comp[0] : "" ?>">
                            </div>
                        <?php

                        }
                        for ($i = count($tabComp) + 1; $i <= 10; $i++) { ?>
                            <div class="skill-tag" style="display: none">
                                <button class="delete-btn" name="skills<?php print $i ?>"><i class="fa-solid fa-xmark"></i></button>
                                <input type="hidden" class="hiddenTag" name="skills<?php print $i ?>" value="">
                            </div>
                        <?php  }
                    } else {
                        for ($i = 1; $i <= 10; $i++) { ?>
                            <div class="skill-tag" style="display: none">
                                <button class="delete-btn" name="skills<?php print $i ?>"><i class="fa-solid fa-xmark"></i></button>
                                <input type="hidden" class="hiddenTag" name="skills<?php print $i ?>" value="">
                            </div>
                    <?php }
                    } ?>

                </div>
            </div>
            <div class="mb-3">
                <input type="url" name="website" class="form-control" id="website" placeholder="Site internet" value="<?php isset($id) ? print $tabCards[0][12] : print "" ?>">
            </div>
            <div class="mb-3">
                <input type="url" name="linkedin" class="form-control" id="linkedin" placeholder="Lien vers le profil Linkedin" value="<?php isset($id) ? print $tabCards[0][13] : print "" ?>">
            </div>
            <div class="mb-3">
                <input type="url" name="viadeo" class="form-control" id="viadeo" placeholder="Lien vers le profil Viadeo" value="<?php isset($id) ? print $tabCards[0][14] : print "" ?>">
            </div>
            <div class="mb-3">
                <input type="url" name="facebook" class="form-control" id="facebook" placeholder="Lien vers le profil Facebook" value="<?php isset($id) ? print $tabCards[0][15] : print "" ?>">
            </div>
            <div class="mb-3">
                <label for="cv" class="form-label ps-1">CV à transmettre</label>
                <input type="file" name="cv" class="form-control" id="cv">
            </div>
            <button type="submit" class="btn btn-success" id="form-button">Envoyer</button>
        </form>
    </div>
    <script src="update.js"></script>
</body>